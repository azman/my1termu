/*----------------------------------------------------------------------------*/
#include "my1uart.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#ifndef PROGVERS
#define PROGVERS "dev-build"
#endif
#define PROGNAME "my1termu"
#define FILENAME_LEN 80
/*----------------------------------------------------------------------------*/
#define ERROR_GENERAL -1
#define ERROR_PARAM_PORT -2
#define ERROR_PARAM_BAUD -3
#define ERROR_PARAM_DEVN -4
#define ERROR_PARAM_WHAT -5
/*----------------------------------------------------------------------------*/
#define OPT_LOCALECHO 0x00000001
#define OPT_SHOWHEX 0x00000002
#define OPT_CR4LF 0x00000004
#define OPT_RAWMODE 0x00000008
/*----------------------------------------------------------------------------*/
#define XMODEM_HEAD_SIZE 3
#define XMODEM_PACK_SIZE 128
#define XMODEM_CSUM_SIZE 1
#define XMODEM_BUFF_SIZE 132
/*----------------------------------------------------------------------------*/
#define ASCII_SOH 0x01
#define ASCII_ACK 0x06
#define ASCII_NAK 0x15
#define ASCII_EOT 0x04
/*----------------------------------------------------------------------------*/
#define SEND_MODE_NORMAL 0
#define SEND_MODE_RETURN 1
#define SEND_QUEUE_SIZE 80
/*----------------------------------------------------------------------------*/
typedef unsigned int termopts_t;
/*----------------------------------------------------------------------------*/
void about(void) {
	printf("Use: %s [options]\n",PROGNAME);
	printf("Options are:\n");
	printf("  --port <number> : port number between 1-%d.\n",MAX_COM_PORT);
	printf("  --baud <number> : baudrate e.g. 9600(default),38400,115200.\n");
	printf("  --tty <device>  : alternate device name (useful in Linux).\n");
	printf("  --help  : show this message. overrides other options.\n");
	printf("  --scan  : scan and list ports. overrides other options.\n");
	printf("  --qsend : use a send queue. does not work in raw mode.\n");
	printf("  --echo  : generate local echo.\n");
	printf("  --num   : display number (hex) instead of text.\n");
	printf("  --raw   : raw keystroke. command function keys disabled.\n");
	printf("  --dos   : msdos terminal - send cr after lf.\n\n");
}
/*----------------------------------------------------------------------------*/
void print_portscan(my1uart_t* port) {
	int test, size = 0;
	printf("--------------------\n");
	printf("COM Port Scan Result\n");
	printf("--------------------\n");
	for (test=1;test<=MAX_COM_PORT;test++) {
		if (uart_prep(port,test)) {
			printf("%s: Ready\n",port->temp);
			size++;
		}
	}
	printf("\nDetected Port(s): %d\n\n",size);
}
/*----------------------------------------------------------------------------*/
void change_baudrate(my1uart_t* port, my1uart_conf_t* conf, int next) {
	int baudrate;
	uart_get_config(port,conf);
	baudrate = uart_actual_baudrate(conf->baud);
	printf("-- Current baudrate: %d\n",baudrate);
	if (next>0) {
		switch (conf->baud) {
			case MY1BAUD1200:
			case MY1BAUD2400:
			case MY1BAUD9600:
			case MY1BAUD19200:
			case MY1BAUD38400:
			case MY1BAUD57600:
				conf->baud++;
				break;
			case MY1BAUD4800:
				conf->baud = MY1BAUD9600;
				break;
			case MY1BAUD115200:
				printf("@@ Already at maximum supported baudrate!\n");
				baudrate = 0;
				break;
		}
	}
	else if (next<0) {
		switch(conf->baud) {
			case MY1BAUD2400:
			case MY1BAUD4800:
			case MY1BAUD19200:
			case MY1BAUD38400:
			case MY1BAUD57600:
			case MY1BAUD115200:
				conf->baud--;
				break;
			case MY1BAUD9600:
				conf->baud = MY1BAUD4800;
				break;
			case MY1BAUD1200:
				printf("@@ Already at minimum supported baudrate!\n");
				baudrate = 0;
				break;
		}
	}
	else baudrate = 0;
	/* only if changing... */
	if (baudrate) {
		baudrate = uart_actual_baudrate(conf->baud);
		printf("-- Setting baudrate: %d\n",baudrate);
		uart_set_config(port,conf);
		/* try to reopen */
		uart_done(port);
		if (uart_open(port))
			uart_purge(port); /* clear input buffer */
		else
			printf("** Cannot re-open port '%s'!\n",port->temp);
	}
	printf("\n");
}
/*----------------------------------------------------------------------------*/
#define KEY_LF 0x0A
#define KEY_CR 0x0D
/*----------------------------------------------------------------------------*/
void sendprint(my1uart_t* port, int data, termopts_t opts) {
	if ((opts&OPT_CR4LF)&&data==KEY_LF) { /* newline '\n' */
		uart_send_byte(port,KEY_CR); /* carriage-return '\r' */
		if (opts&OPT_LOCALECHO) {
			if (opts&OPT_SHOWHEX) printf("[%02X]",KEY_CR);
			else putchar(KEY_CR);
		}
	}
	uart_send_byte(port,data); /* send */
	if (opts&OPT_LOCALECHO) {
		if (opts&OPT_SHOWHEX) printf("[%02X]",data);
		else putchar(data);
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1uart_t port;
	my1uart_conf_t conf;
	my1key_t key, esc;
	termopts_t opts = 0x0;
	int term = 1, baud = 0, scan = 0, send = SEND_MODE_NORMAL;
	int test, loop, mask, drag, done = 0;
	int pin_dtr = 1, pin_rts = 1;
	char name[FILENAME_LEN], *ptty = 0x0;
	char send_queue[SEND_QUEUE_SIZE];
	int send_qsize = 0;
	FILE* pfile;
	/* using unbuffered output */
	setbuf(stdout,0);
	/* print tool info */
	printf("\n%s - Simple Serial Port Interface (%s) "
		"by azman@my1matrix.org\n\n",PROGNAME,PROGVERS);
	/* check program arguments */
	if (argc>1) {
		for (loop=1;loop<argc;loop++) {
			if (argv[loop][0]=='-') { /* options! */
				if (!strcmp(argv[loop],"--port")) {
					if (get_param_int(argc,argv,&loop,&test)<0) {
						printf("Cannot get port number!\n\n");
						return ERROR_PARAM_PORT;
					}
					else if (test>MAX_COM_PORT) {
						printf("Invalid port number! (%d)\n\n", test);
						return ERROR_PARAM_PORT;
					}
					term = test;
				}
				else if (!strcmp(argv[loop],"--baud")) {
					if (get_param_int(argc,argv,&loop,&test)<0) {
						printf("Cannot get baud rate!\n\n");
						return ERROR_PARAM_BAUD;
					}
					baud = test;
				}
				else if (!strcmp(argv[loop],"--tty")) {
					if (!(ptty=get_param_str(argc,argv,&loop))) {
						printf("Error getting tty name!\n\n");
						return ERROR_PARAM_DEVN;
					}
				}
				else if (!strcmp(argv[loop],"--help")) {
					about();
					return 0;
				}
				else if (!strcmp(argv[loop],"--scan")) scan = 1;
				else if (!strcmp(argv[loop],"--qsend"))
					send = SEND_MODE_RETURN;
				else if (!strcmp(argv[loop],"--echo"))
					opts |= OPT_LOCALECHO; /* local echo */
				else if (!strcmp(argv[loop],"--num"))
					opts |= OPT_SHOWHEX; /* numbers not text */
				else if (!strcmp(argv[loop],"--raw"))
					opts |= OPT_RAWMODE; /* raw keystroke */
				else if (!strcmp(argv[loop],"--dos"))
					opts |= OPT_CR4LF; /* ms-dos lf cr */
				else {
					printf("Unknown option '%s'!\n",argv[loop]);
					return ERROR_PARAM_WHAT;
				}
			} else { /* not an option? */
				printf("Unknown parameter %s!\n",argv[loop]);
				return ERROR_PARAM_WHAT;
			}
		}
	}
	/* initialize port */
	uart_init(&port);
	/* check user requested name change */
	if (ptty) uart_name(&port,ptty);
	/* check if user requested a port scan */
	if (scan) {
		print_portscan(&port);
		return 0;
	}
	/* try to prepare port with requested terminal */
	if (!term) term = uart_find(&port,0x0);
	if (!uart_prep(&port,term)) {
		about();
		print_portscan(&port);
		printf("Cannot prepare port '%s%d'!\n\n",port.name,COM_PORT(term));
		return ERROR_GENERAL;
	}
	/* apply custom baudrate */
	if (baud) {
		if (!uart_set_baudrate(&port,baud))
			printf("-- Invalid baudrate (%d)! ",baud);
		printf("-- Baudrate: %d\n",uart_get_baudrate(&port));
	}
	/* try opening port */
	if (!uart_open(&port)) {
		printf("Cannot open port '%s'!\n\n",port.temp);
		return ERROR_GENERAL;
	}
	/* check control pin DTR/RTS status */
	if (uart_pin_detect(&port,MY1PIN_DTR,&pin_dtr))
		printf("-- Cannot read DTR pin status!\n");
	if (uart_pin_detect(&port,MY1PIN_RTS,&pin_rts))
		printf("-- Cannot read RTS pin status!\n");
	/* clear input buffer */
	uart_purge(&port);
	/* start main loop */
	while (1) {
		key = get_keyhit_extended(&esc);
		if (key!=KEY_NONE) {
			/* if we are in raw mode, simply send raw key hit */
			if (opts&OPT_RAWMODE) {
				if (key<0x100) /* 8-bit ascii */
					sendprint(&port,key,opts);
				else {
					/* send all bytes! */
					mask = 0xFF000000;
					drag = 24;
					while (mask) {
						test = mask & key;
						if (test) {
							test >>= drag;
							sendprint(&port,test,opts);
						}
						drag -= 8;
						mask >>= 8;
					}
				}
				if (esc==KEY_F12) {
					opts &= ~OPT_RAWMODE;
					printf("\n\nSwitching to Command Mode!\n\n");
				}
				continue;
			}
			/* process control keys? */
			switch (esc) {
				case KEY_F1: /* show commands */
					printf("\n\n------------\n");
					printf("Command Keys\n");
					printf("------------\n");
					printf("<F1>     - Show this help\n");
					printf("<F2>     - Send file (RAW)\n");
					printf("<F3>     - Send file (XMODEM)\n");
					printf("<F4>     - Show current settings\n");
					printf("<F5>     - Toggle local echo\n");
					printf("<F6>     - Toggle LF4CR option (Win32)\n");
					printf("<F7>     - Toggle Hex display mode\n");
					printf("<F8>     - Decrease baudrate\n");
					printf("<F9>     - Increase baudrate\n");
					printf("<F10>    - Exit this program\n");
					printf("<F11>    - Clear input/output buffer\n");
					printf("<F12>    - Toggle command/raw mode\n");
					printf("<ALT+p> - Purge Send Queue\n");
					printf("<ALT+s> - Toggle Send Mode\n");
					printf("<ALT+r> - Insert newline into Send Queue\n");
					printf("<ALT+z> - Send file (Hexadecimal)\n");
					printf("<CTRL+d> - Toggle DTR Line\n");
					printf("<CTRL+r> - Toggle RTS Line\n");
					printf("<CTRL+t> - Check DSR/CTS Status\n\n");
					break;
				case KEY_F2: /* send file RAW */
					printf("\n\nSend file (RAW)");
					printf("\nFile: ");
					if (!fgets(name,FILENAME_LEN,stdin)||name[0]=='\n') {
						printf("\nNo valid input!\n\n");
						continue;
					}
					/* have to filter out newline */
					for (loop=0;loop<FILENAME_LEN;loop++) {
						if(name[loop]=='\n'||name[loop]=='\r'||
								name[loop]==0x0) {
							name[loop] = 0x0;
							break;
						}
					}
					pfile = fopen(name,"rb");
					if (pfile==NULL)
						printf("\nFile \'%s\' not found!\n\n",name);
					else {
						while (!feof(pfile)) {
							test = fgetc(pfile);
							sendprint(&port,test,opts);
						}
						fclose(pfile);
						printf("\nFile \'%s\' sent!\n\n",name);
					}
					break;
				case KEY_F3: /* send file XMODEM */
					printf("\n\nSend file (XMODEM)");
					printf("\nFile: ");
					if(!fgets(name,FILENAME_LEN,stdin)||name[0]=='\n') {
						printf("\nNo valid input!\n\n");
						continue;
					}
					/* have to filter out newline */
					for (loop=0;loop<FILENAME_LEN;loop++) {
						if(name[loop]=='\n'||name[loop]=='\r'||
								name[loop]==0x0) {
							name[loop] = 0x0;
							break;
						}
					}
					pfile = fopen(name,"rb");
					if(pfile==NULL)
						printf("\nFile \'%s\' not found!\n\n",name);
					else {
						int count = 1, index, check, error = 0;
						unsigned char buff[XMODEM_BUFF_SIZE];
						do {
							uart_purge(&port);
							/* sync with a NAK */
							while (1) {
								key = get_keyhit();
								if (key==KEY_ESCAPE) {
									printf("\nUser abort!\n\n");
									error = 1;
									break;
								}
								if (uart_incoming(&port)&&
										uart_read_byte(&port)==ASCII_NAK)
									break;
							}
							if (error) break;
							printf("\nSending file '%s'...",name);
							while (1) {
								/* pack data */
								buff[0] = ASCII_SOH;
								buff[1] = count;
								buff[2] = 0xFF - count;
								check = buff[0] + buff[1] + buff[2];
								index = 0; loop = 3;
								while (!feof(pfile)&&index<XMODEM_PACK_SIZE) {
									test = fgetc(pfile);
									buff[loop] = test & 0xFF;
									check += buff[loop];
									index++; loop++;
								}
								/* zero pad final packet */
								while (loop<XMODEM_BUFF_SIZE-1) {
									buff[loop] = 0;
									check += buff[loop];
									loop++;
								}
								/* insert crc byte */
								buff[XMODEM_BUFF_SIZE-1] = check & 0xFF;
								/* send data */
								uart_purge(&port);
								for (loop=0;loop<XMODEM_BUFF_SIZE;loop++)
									uart_send_byte(&port,buff[loop]);
								/* must get ack - else, an error? */
								while (!uart_incoming(&port));
								if (uart_read_byte(&port)!=ASCII_ACK) {
									printf("\nSend error!\n\n");
									error = 1;
									break;
								}
								else putchar('.');
								/* check if last packet? */
								if (index<XMODEM_PACK_SIZE||feof(pfile)) {
									/* send EOT */
									uart_send_byte(&port,ASCII_EOT);
									/* must get ack - else, an error? */
									while (!uart_incoming(&port));
									if (uart_read_byte(&port)!=ASCII_ACK) {
										printf("\nEOT error!\n\n");
										error = 1;
									}
									else printf(" done!\n");
									break;
								}
								count++;
								if (count>255) count = 1;
							}
						} while (0); /* dummy loop! */
						fclose(pfile);
					}
					break;
				case KEY_F4: /* show current settings */
					printf("\n\nProgram '%s' - Current Settings:\n",PROGNAME);
					if (uart_isopen(&port))
						printf("\nConnected to '%s'!",port.temp);
					else printf("\nNOT connected?!");
					if (opts&OPT_LOCALECHO) printf("\nLocal echo ON!");
					else printf("\nLocal echo OFF!");
					if (opts&OPT_CR4LF) printf("\nCR4LF option ON!");
					else printf("\nCR4LF option OFF!");
					if (opts&OPT_SHOWHEX) printf("\nHex Display Mode ON!");
					else printf("\nHex Display Mode OFF!");
					baud = uart_get_baudrate(&port);
					printf("\nEffective baudrate: %d",baud);
					printf("\nSend Mode: %s",
						send==SEND_MODE_RETURN?"ON-RETURN":"NORMAL");
					send_queue[send_qsize] = 0x0;
					printf("\nSend Queue: '%s'\n\n",send_queue);
					break;
				case KEY_F5: /* toggle local echo */
					opts ^= OPT_LOCALECHO;
					if (opts&OPT_LOCALECHO) printf("\n\nLocal echo ON!\n\n");
					else printf("\n\nLocal echo OFF!\n\n");
					break;
				case KEY_F6: /* toggle win32 newline option */
					opts ^= OPT_CR4LF;
					if (opts&OPT_CR4LF) printf("\n\nLF4CR option ON!\n\n");
					else printf("\n\nLF4CR option OFF!\n\n");
					break;
				case KEY_F7: /* toggle hex display */
					opts ^= OPT_SHOWHEX;
					if (opts&OPT_SHOWHEX)
						printf("\n\nHex Display Mode ON!\n\n");
					else printf("\n\nHex Display Mode OFF!\n\n");
					break;
				case KEY_F8: /* decrease baudrate */
					change_baudrate(&port,&conf,-1);
					break;
				case KEY_F9: /* increase baudrate */
					change_baudrate(&port,&conf,1);
					break;
				case KEY_F10:
					printf("\n\nUser Exit Request! Program '%s' Ends.\n\n",
						PROGNAME);
					done = 1;
					break;
				case KEY_F11: /* clear input/output buffer */
					printf("\nPurging input buffer!\n");
					uart_purge(&port);
					printf("Flushing output buffer!\n\n");
					uart_flush(&port);
					break;
				case KEY_F12: /* toggle raw mode */
					opts |= OPT_RAWMODE;
					printf("\n\nSwitching to Raw Mode!\n\n");
					break;
				case KEY_NONE:
					/* normal keystroke? */
					if (key==KEY_CTRL_d) {
						pin_dtr = !pin_dtr;
						if (uart_pin_assert(&port,MY1PIN_DTR,pin_dtr))
							printf("\nCannot set DTR pin to %d!\n",pin_dtr);
						else
							printf("\nDTR pin set to %d!\n",pin_dtr);
					}
					else if(key==KEY_CTRL_r) {
						pin_rts = !pin_rts;
						if (uart_pin_assert(&port,MY1PIN_RTS,pin_rts))
							printf("\nCannot set RTS pin to %d!\n",pin_rts);
						else
							printf("\nRTS pin set to %d!\n",pin_rts);
					}
					else if (key==KEY_CTRL_t) {
						if (uart_pin_detect(&port,MY1PIN_DSR,&test))
							printf("\nCannot read DSR pin status!");
						else
							printf("\nDSR pin status: %08X!",test);
						if (uart_pin_detect(&port,MY1PIN_CTS,&test))
							printf("\nCannot read CTS pin status!");
						else
							printf("\nCTS pin status: %08X!",test);
						if (uart_pin_detect(&port,MY1PIN_DTR,&test))
							printf("\nCannot read DTR pin status!");
						else
							printf("\nDTR pin status: %08X!",test);
						if (uart_pin_detect(&port,MY1PIN_RTS,&test))
							printf("\nCannot read RTS pin status!");
						else
							printf("\nRTS pin status: %08X!",test);
						printf("\n");
					}
					else {
						if (send==SEND_MODE_RETURN) {
							if (key==KEY_ENTER) {
								if (send_qsize>0) {
									send_queue[send_qsize] = 0x0;
									printf("\nSending text queue: %s (%d)\n",
										send_queue,send_qsize);
									for (loop=0;loop<send_qsize;loop++)
										sendprint(&port,send_queue[loop],
											opts);
									/**sendprint(&port,KEY_ENTER,opts);*/
									send_qsize = 0;
								}
								else putchar(key);
							}
							else {
								send_queue[send_qsize++] = key;
								if (send_qsize>=SEND_QUEUE_SIZE-1) {
									send_queue[send_qsize] = 0x0;
									printf("\nPurging text queue: %s (%d/%d)\n",
										send_queue,send_qsize,SEND_QUEUE_SIZE);
									send_qsize = 0;
								}
								else putchar(key);
							}
						}
						else sendprint(&port,key,opts);
					}
					break;
				default:
					if (key==KEY_ESCAPE) {
						if (esc==(my1key_t)'p'||esc==(my1key_t)'P') {
							/* alt+p => purge send queue */
							if (send_qsize>0) {
								send_queue[send_qsize] = 0x0;
								printf("\nPurge text queue: %s\n",send_queue);
								send_qsize = 0;
							}
						}
						else if (esc==(my1key_t)'s'||esc==(my1key_t)'S') {
							 /* alt+s => toggle send mode */
							if (send==SEND_MODE_RETURN) {
								send = SEND_MODE_NORMAL;
								printf("\nSend Mode: NORMAL\n");
							}
							else {
								send = SEND_MODE_RETURN;
								printf("\nSend Mode: ON-RETURN");
							}
						}
						else if (esc==(my1key_t)'r'||esc==(my1key_t)'R') {
							 /* alt+r => insert newline in queue */
							if (send==SEND_MODE_RETURN&&
									send_qsize<SEND_QUEUE_SIZE-1) {
								printf("\nNewline inserted into send queue\n");
								send_queue[send_qsize++] = KEY_ENTER;
							}
						}
						else if (esc==(my1key_t)'z'||esc==(my1key_t)'Z') {
							 /* alt+z => send hexadecimal file */
							printf("\n\nSend data (hexadecimal)");
							printf("\nFile: ");
							if (!fgets(name,FILENAME_LEN,stdin)||
									name[0]=='\n') {
								printf("\nNo valid input!\n\n");
								continue;
							}
							/* have to filter out newline */
							for (loop=0;loop<FILENAME_LEN;loop++) {
								if (name[loop]=='\n'||name[loop]=='\r'||
										name[loop]==0x0) {
									name[loop] = 0x0;
									break;
								}
							}
							pfile = fopen(name,"rb");
							if (!pfile)
								printf("\nFile '%s' not found!\n\n",name);
							else {
								printf("\nSending data in '%s': ",name);
								while (!feof(pfile)) {
									if (fscanf(pfile,"%x",&test)==1) {
										uart_send_byte(&port,test);
										printf("[0x%02x]",test);
									}
								}
								fclose(pfile);
								printf("\nDone!\n\n");
							}
						}
						else if ((esc>=0x41&&esc<0x5B)||
								(esc>=0x61&&esc<0x7B)||(esc>=0x30&&esc<0x39)) {
							/* ignore other alt+<key> ??? */
						}
						else {
							/* ignore other extended keys ??? */
						}
					}
					break;
			}
			/* break in switch :( */
			if (done) break;
		}
		/* check serial port for incoming data */
		if (uart_incoming(&port)) {
			test = uart_read_byte(&port);
			if (opts&OPT_SHOWHEX) printf("[%02X]",test);
			else putchar(test);
		}
		/* check port status */
		if (!uart_okstat(&port)) {
			printf("Port Error (%d)! Aborting!\n",port.stat);
			break;
		}
	}
	/* close port */
	uart_done(&port);
	return 0;
}
/*----------------------------------------------------------------------------*/
