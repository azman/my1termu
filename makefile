# makefile for my1termu - a cross-platform serial port interface

PROJECT = my1termu
CONSBIN = $(PROJECT)
CONSPRO = $(CONSBIN)
CONSOBJ = my1keys.o my1uart.o $(CONSBIN).o
VERSION = -DPROGVERS=\"$(shell date +%Y%m%d)\"

DELETE = rm -rf

CFLAGS += -Wall -static
LFLAGS += -s
OFLAGS +=

EXTPATH = ../my1codelib/src
CFLAGS += -I$(EXTPATH)

ifeq ($(OS),Windows_NT)
DO_MINGW=YES
endif

ifeq ($(DO_MINGW),YES)
	CONSPRO = $(CONSBIN).exe
ifneq ($(OS),Windows_NT)
	# cross-compiler settings
	XTOOL_DIR ?= /home/share/tool/mingw
	XTOOL_TARGET = $(XTOOL_DIR)
	CROSS_COMPILE = $(XTOOL_TARGET)/bin/i686-pc-mingw32-
	# extra switches - do i need these???
	CFLAGS += -I$(XTOOL_DIR)/include
	LFLAGS += -L$(XTOOL_DIR)/lib
endif
	# tell sources this is mingw build
	CFLAGS += -DDO_MINGW
endif

PACKDAT += $(CONSPRO)
CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++
debug: CFLAGS += -DMY1DEBUG
version: VERSION = -DPROGVERS=\"$(shell cat VERSION)\"

main: $(CONSPRO)

new: clean main

debug: new

version: new

$(CONSPRO): $(CONSOBJ)
	$(CC) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(VERSION) -c $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	-$(DELETE) $(CONSBIN) *.exe *.bz2 *.zip *.o
